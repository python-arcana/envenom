<!-- `envenom` - an elegant application configurator for the more civilized age
Copyright (C) 2024 Artur Ciesielski <artur.ciesielski@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>. -->

## Problem statement

How many times have you written a piece of code like this, going through all the
steps every single time when starting a new Django project?

Let's take a look at the `myproject/settings.py` file:

```python
...

import os

from django.core.exceptions import ImproperlyConfigured


def env(name: str, default: str | None = None) -> str:
    if (value := os.environ.get(name, default)) is None:
        raise ImproperlyConfigured(name)
    return value

...

SECRET_KEY = env("SECRET_KEY")

...
```

Alright, simple, it works and type checks correctly. `SECRET_KEY` will always be set
and it will always be a `str`.

Now we want to add some optional settings:

```python
...

# custom settings
SERVICE_PORT = env("SERVICE_PORT", ???)

# @fixme: how to make it optional?!

# here it's required, will raise an exception
SERVICE_PORT = env("SERVICE_PORT", None)
# here it's required, with a default of an empty string instead of None
# (to the type system's horror)
SERVICE_PORT = env("SERVICE_PORT", "")

...
```

Can't use `None` to signal "return nothing on no value" and "error out on no value"
at the same time. Makes sense.

Ok, maybe use a sentinel value for the `default` parameter in the `env` function?

```python
...

import os

from django.core.exceptions import ImproperlyConfigured


class __Sentinel:
    pass


def env(name: str, default: str | None | __Sentinel = __Sentinel()) -> str | None:
    value = os.environ.get(name, default)
    if isinstance(value, __Sentinel):
        raise ImproperlyConfigured(name)
    return value

...

SECRET_KEY = env("SECRET_KEY")

...

SERVICE_PORT = env("SERVICE_PORT", None)

...
```

Now it works as intended, even though it's a little smelly, what even is a sentinel.

...

Turns out the custom optional setting really needs to be an `int`. Of course environment
variable values are all `str`s, so let's map it:

```python
...

SERVICE_PORT = int(v) if (v := env("SERVICE_PORT", None)) else None

...
```

That's an ugly line. We'll split it I guess.

Now we only need to ensure the values are usable - maybe catch some `ValueError`s
when converting:

```python
...


v = env("SERVICE_PORT", None)
try:
    SERVICE_PORT = int(v) if v is not None else None
except ValueError as e:
    raise ImproperlyConfigured("SERVICE_PORT", v) from e

...
```

Wow, that is bad - do we have to do that for every non-`str` setting we'd like to use?
Maybe we should write another wrapper to catch the conversion error?

Now we need to consider a simple case of feature flags - either it's enabled, or it's not.
We'd optimally use `bool`s for that, so let's write a simple convenience helper to convert
`str`s to `bool`s semantically:

```python
def convert_to_bool(v: str) -> bool:
    normalized = v.lower()
    if normalized in {"t", "true", "y", "yes", "1", "+", "✓"}:
        return True
    if normalized in {"f", "false", "n", "no", "0", "-", "✗"}:
        return False
    raise ValueError(v)
```

And we're ready to set some feature flags:

```python
...

v = env("FEATURE_FLAG", False)
try:
    FEATURE_FLAG: bool | None = convert_to_bool(v) if isinstance(v, str) else v
except ValueError as e:
    raise ImproperlyConfigured("FEATURE_FLAG", v) from e

...
```

And there's nowhere to apply the converter, we need to make an `isinstance` check to ensure
we're not applying it to a value that is already a `bool`. Yikes.

Anyway it's done, it's a little nightmare and it looks terrible and is barely usable
but we've covered all usecases. We can clean up a little bit here with using generic types...

...

*FUCK!*

The `SECRET_KEY` is now marked by the type checker as `str | None`.


### Final Django example

```python
import os
from typing import TypeVar

from django.core.exceptions import ImproperlyConfigured


class __Sentinel:
    pass


T = TypeVar("T")


def env(name: str, default: T | __Sentinel | None = __Sentinel()) -> str | T | None:
    value = os.environ.get(name, default)
    if isinstance(value, __Sentinel):
        raise ImproperlyConfigured(name)
    return value


def convert_to_bool(v: str) -> bool:
    """
    Helper function for converting strings to booleans semantically.
    `bool` doesn't do it like `int` does.
    """

    normalized = v.lower()
    if normalized in {"t", "true", "y", "yes", "1", "+", "✓"}:
        return True
    if normalized in {"f", "false", "n", "no", "0", "-", "✗"}:
        return False
    raise ValueError(v)


# will always be `str`, but marked by the type checker as `str | None`!
SECRET_KEY: str | None = env("SECRET_KEY")

v = env("SERVICE_PORT", None)
try:
    # this works just about correctly
    SERVICE_PORT: int | None = int(v) if v is not None else None
except ValueError as e:
    raise ImproperlyConfigured("SERVICE_PORT", v) from e

v = env("FEATURE_FLAG", False)
try:
    # `isinstance` is a deal-breaker here
    FEATURE_FLAG: bool | None = convert_to_bool(v) if isinstance(v, str) else v
except ValueError as e:
    raise ImproperlyConfigured("FEATURE_FLAG", v) from e
```


### Final Django example with `envenom`

```python
from django.core.exceptions import ImproperlyConfigured

from envenom import config, optional, required, with_default
from envenom.errors import ConfigurationError
from envenom.parsers import make_bool_parser


@config()
class DjangoCfg:
    secret_key: str = required()
    service_port: int | None = optional(int)
    feature_flag: bool = with_default(make_bool_parser(), default=False)


try:
    cfg = DjangoCfg()
except ConfigurationError as e:
    raise ImproperlyConfigured(*e.args) from e


# type annotations here are unnecessary, they will be inferred by the type checker
SECRET_KEY: str = cfg.secret_key
SERVICE_PORT: int | None = cfg.service_port
FEATURE_FLAG: bool = cfg.feature_flag
```

## Comparison to similar libraries

### `environment-variables`

[https://pypi.org/project/environment-variables](https://pypi.org/project/environment-variables)

`envenom` is very similar in spirit to `environment-variables`. Unlike `environment-variables`,
which rely on Enum-style access and relevant metaprogramming, `envenom` trims down the required
metaprogramming to an absolute minimum, hiding most of the ugliness behind the standard Python
dataclass interface. `envenom` is similarly lightweight, but goes a step further. `envenom` also
provides support for namespacing variables, built-in `bool` and `list` parsers and the
secrets-mounted-as-files idiom.

Even our `rationale` sections are very similar. :)

### `environ-config`

[https://pypi.org/project/environ-config](https://pypi.org/project/environ-config)

`envenom` is also very similar to `environ-config`. However `environ-config` is much broader
in scope, allowing users to pull configuration directly from cloud providers' secret storages and
local configuration files. `envenom` aims to cover a lesser scope, but requires much less complex
metaprogramming behind the scenes. `envenom` is also based on native dataclasses instead of `attrs`
and provides support for built-in `bool` and `list` parsers and the secrets-mounted-as-files idiom.
