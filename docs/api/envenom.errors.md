<!-- `envenom` - an elegant application configurator for the more civilized age
Copyright (C) 2024 Artur Ciesielski <artur.ciesielski@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>. -->

All `envenom` errors should have sane default representations when `str`ed,
`repr`ed and used in format strings. All error classes also provide `class`
and `var` format specs, while `InvalidConfiguration` and its subclasses
also provide the `value` format spec.

::: envenom.errors.GenericError

::: envenom.errors.ConfigurationError

::: envenom.errors.MissingConfiguration

::: envenom.errors.InvalidConfiguration

::: envenom.errors.ConfigurationFileUnreadable
