<!-- `envenom` - an elegant application configurator for the more civilized age
Copyright (C) 2024 Artur Ciesielski <artur.ciesielski@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>. -->

## Overview

A very popular idiom recently (especially with Kubernetes deployments) is mounting
secrets or other configuration as a directory of text files, each of them representing
a single configuration field. `envenom` supports this mode of deployment out of the box
with the `__FILE` suffix. If a variable would be configured via `<namespace>__<name>`,
then it can also be configured via `<namespace>__<name>__FILE` instead. This behavior
can be disabled by specifying `file=False` in the field configuration, if so desired.
If both environment variables are set, the file will take precedence over the
standard variable value.

## Example

```python
from envenom import config, required, optional


@config()
class MainCfg:
    some_value: str = required()
    other_value: str | None = optional(file=False)


if __name__ == "__main__":
    cfg = MainCfg()

    print(f"cfg.some_value ({type(cfg.some_value)}): {repr(cfg.some_value)}")
    print(f"cfg.other_value ({type(cfg.other_value)}): {repr(cfg.other_value)}")
```

Create a file with the variable's value and run the example with the environment set:

```bash
export SOME_VALUE__FILE=$(mktemp) && \
export OTHER_VALUE__FILE=$(mktemp) && \
echo "value1" > ${SOME_VALUE__FILE} && \
echo "value2" > ${OTHER_VALUE__FILE} && \
python -m envenom.examples.advanced.files
```

```text
cfg.some_value (<class 'str'>): 'value1'
cfg.other_value (<class 'NoneType'>): None
```
