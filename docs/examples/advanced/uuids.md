<!-- `envenom` - an elegant application configurator for the more civilized age
Copyright (C) 2024 Artur Ciesielski <artur.ciesielski@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>. -->

## Example

This example shows how to use UUIDs as config fields.
It is available in the `envenom.examples.advanced.uuids` runnable module.

```python
from uuid import UUID, uuid4

from envenom import config, optional, required, with_default_factory


@config()
class UUIDCfg:
    admin_user_uuid: UUID = required(UUID)
    manager_user_uuid: UUID | None = optional(UUID)
    random_uuid_if_unset: UUID = with_default_factory(UUID, default_factory=uuid4)


if __name__ == "__main__":
    cfg = UUIDCfg()

    print(f"cfg.admin_user_uuid ({type(cfg.admin_user_uuid)}): {repr(cfg.admin_user_uuid)}")
    print(f"cfg.manager_user_uuid ({type(cfg.manager_user_uuid)}): {repr(cfg.manager_user_uuid)}")
    print(f"cfg.random_uuid_if_unset ({type(cfg.random_uuid_if_unset)}): {repr(cfg.random_uuid_if_unset)}")
```

Run the example with the environment set:

```bash
ADMIN_USER_UUID="cde82330-7bd9-4689-b741-86662619cdaf" \
python -m envenom.examples.advanced.uuids
```

```text
cfg.admin_user_uuid (<class 'uuid.UUID'>): UUID('cde82330-7bd9-4689-b741-86662619cdaf')
cfg.manager_user_uuid (<class 'NoneType'>): None
cfg.random_uuid_if_unset (<class 'uuid.UUID'>): UUID('1df11225-10ff-4f32-a49d-9606f5f1d964')
```
