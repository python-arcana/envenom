<!-- `envenom` - an elegant application configurator for the more civilized age
Copyright (C) 2024 Artur Ciesielski <artur.ciesielski@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>. -->

## Example

This example shows how to integrate `envenom` with a FastAPI app.
The FastAPI ASGI app is available as `envenom.examples.fastapi:app`.

As an additional dependency install `FastAPI==0.111.0`,
and also a webserver (here `uvicorn==0.30.1`).

```bash
python -m pip install FastAPI==0.111.0 uvicorn==0.30.1
```

```python
from dataclasses import dataclass

from fastapi import FastAPI
from fastapi.routing import APIRoute

from envenom import config, with_default
from envenom.parsers import make_bool_parser


@config()
class FastAPICfg:
    static_docs: bool = with_default(make_bool_parser(), default=True)
    interactive_docs: bool = with_default(make_bool_parser(), default=False)


@config(namespace="app")
class AppCfg:
    motd: str = with_default(default="This is the message of the day.")


@dataclass
class IndexResponse:
    motd: str


async def get_index() -> IndexResponse:
    return IndexResponse(motd=AppCfg().motd)


cfg = FastAPICfg()
app = FastAPI(
    title="envenom.examples.fastapi",
    summary="An elegant application configurator for the more civilized age",
    redoc_url="/redoc" if cfg.static_docs else None,
    docs_url="/docs" if cfg.interactive_docs else None,
    routes=[
        APIRoute("/", get_index, methods={"GET"}),
    ],
)
```

Run the example with:

```bash
python -m uvicorn envenom.examples.fastapi:app --host '127.0.0.1' --port '8000'
```

Visit:

- [http://localhost:8000/](http://localhost:8000/) to see the MOTD.
- [http://localhost:8000/redoc](http://localhost:8000/redoc) to see the static docs, if enabled.
- [http://localhost:8000/docs](http://localhost:8000/docs) to see the interactive docs, if enabled.

Experiment with different settings for:

- `APP__MOTD`
- `STATIC_DOCS`
- `INTERACTIVE_DOCS`
