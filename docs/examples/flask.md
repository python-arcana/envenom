<!-- `envenom` - an elegant application configurator for the more civilized age
Copyright (C) 2024 Artur Ciesielski <artur.ciesielski@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>. -->

## Example

This example shows how to integrate `envenom` with a Flask app.
The Flask WSGI app is available as `envenom.examples.flask:app`.

As an additional dependency install `Flask==3.0.3`.

```bash
python -m pip install Flask==3.0.3
```

```python
from typing import TypedDict

from flask import Flask

from envenom import config, with_default
from envenom.parsers import make_bool_parser


@config()
class FlaskCfg:
    static_docs: bool = with_default(make_bool_parser(), default=True)
    interactive_docs: bool = with_default(make_bool_parser(), default=False)


@config(namespace="app")
class AppCfg:
    motd: str = with_default(default="This is the message of the day.")


cfg = FlaskCfg()
app = Flask(__name__)


class IndexResponse(TypedDict):
    motd: str


@app.route("/")
def index() -> IndexResponse:
    return {"motd": AppCfg().motd}


if cfg.static_docs:

    @app.route("/redoc")
    def static_docs() -> str:
        return "Static docs would be here if Flask had them built-in."

if cfg.interactive_docs:

    @app.route("/docs")
    def interactive_docs() -> str:
        return "Interactive docs would be here if Flask had them built-in."

```

Run the example with:

```bash
FLASK_APP='envenom.examples.flask:app' python -m flask run --host '127.0.0.1' --port '8000'
```

Visit:

- [http://localhost:8000/](http://localhost:8000/) to see the MOTD.
- [http://localhost:8000/redoc](http://localhost:8000/redoc) to see the static docs, if enabled.
- [http://localhost:8000/docs](http://localhost:8000/docs) to see the interactive docs, if enabled.

Experiment with different settings for:

- `APP__MOTD`
- `STATIC_DOCS`
- `INTERACTIVE_DOCS`
