<!-- `envenom` - an elegant application configurator for the more civilized age
Copyright (C) 2024 Artur Ciesielski <artur.ciesielski@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>. -->

## Overview

Namespaces let you prefix your variable names. Each config class can have zero or
more namespaces associated with it. When the config class is instantiated and
values are being pulled from the environment, all namespaces will be glued together
and prefixed to the variable name. All nonsensical characters will be replaced with
`_`'s, namespaces will be joined together with the var name using `__` and everything
will be converted to uppercase.

## Example

This example shows how to use namespaces for reusing the same field base name for
multiple purposes. It is available in the `envenom.examples.namespaces`
runnable module.

```python
from envenom import config, required, subconfig


@config(("app", "cfg"))
class OtherNamespaceCfg:
    some_value: str = required()


@config(("ns1", "ns-2"))
class NestedNamespaceCfg:
    some_value: str = required()


@config("ns1")
class SimpleNamespaceCfg:
    some_value: str = required()
    nested_namespace: NestedNamespaceCfg = subconfig(NestedNamespaceCfg)


@config()
class MainCfg:
    some_value: str = required()
    simple_namespace: SimpleNamespaceCfg = subconfig(SimpleNamespaceCfg)
    other_namespace: OtherNamespaceCfg = subconfig(OtherNamespaceCfg)


if __name__ == "__main__":
    cfg = MainCfg()

    print(f"cfg.some_value ({type(cfg.some_value)}): {repr(cfg.some_value)}")
    print(f"cfg.simple_namespace.some_value ({type(cfg.simple_namespace.some_value)}): {repr(cfg.simple_namespace.some_value)}")
    print(f"cfg.simple_namespace.nested_namespace.some_value ({type(cfg.simple_namespace.nested_namespace.some_value)}): {repr(cfg.simple_namespace.nested_namespace.some_value)}")
    print(f"cfg.other_namespace.some_value ({type(cfg.other_namespace.some_value)}): {repr(cfg.other_namespace.some_value)}")
```

Run the example with the environment set:

```bash
SOME_VALUE="value1" \
NS1__SOME_VALUE="value2" \
NS1__NS_2__SOME_VALUE="value3" \
APP__CFG__SOME_VALUE="value4" \
python -m envenom.examples.namespaces
```

```text
cfg.some_value (<class 'str'>): 'value1'
cfg.simple_namespace.some_value (<class 'str'>): 'value2'
cfg.simple_namespace.nested_namespace.some_value (<class 'str'>): 'value3'
cfg.skip_namespace.some_value (<class 'str'>): 'value4'
```
