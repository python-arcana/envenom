<!-- `envenom` - an elegant application configurator for the more civilized age
Copyright (C) 2024 Artur Ciesielski <artur.ciesielski@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>. -->

## Overview

Parsers allow transforming every declared config field into a non-`str` type.
This allows the whole configuration to be strongly typed and deviations can
easily be found using static type analysis.

There are some notable pitfalls:

- if the config field is declared as `T | None`, then using `required` will
type check correctly - because `T` always satisfies `T | None`

### Built-in parsers

`envenom` offers two standard customizable parsers in the `envenom.parsers` module:

- `make_bool_parser` creates a parser which will parse boolean values semantically.
It's case insensitive and allows customizing the values considered `True` and `False`.
- `make_list_parser` creates a parser which will split a string value into a list
and ensure every element will be processed with another parser. It allows
customizing the underlying value parser and value separator.

## Example

This example shows how to use built in `make_bool_parser` and `make_list_parser`, as well
as custom parsers with a generic container. It is available in the
`envenom.examples.parsers` runnable module.

```python
from dataclasses import dataclass

from envenom import config, optional, required
from envenom.parsers import make_bool_parser, make_list_parser


@dataclass
class Box[T]:
    item: T


def boxed_int(v: str) -> Box[int]:
    return Box(item=int(v))


def boxed_bytes(v: str) -> Box[bytes]:
    return Box(item=v.encode())


@config()
class MainCfg:
    boxed_int: Box[int] = required(boxed_int)
    boxed_bytes: Box[bytes] = required(boxed_bytes)

    default_boolean: bool = required(make_bool_parser())
    custom_boolean: bool = required(
        make_bool_parser(true_values={"mhm"}, false_values={"uhhuh"})
    )

    required_list: list[str] = required(make_list_parser())
    required_empty_list: list[str] = required(make_list_parser())
    optional_list: list[str] | None = optional(make_list_parser())
    optional_empty_list: list[str] | None = optional(make_list_parser())
    optional_not_provided_list: list[str] | None = optional(make_list_parser())
    custom_list: list[int] = required(make_list_parser(int, separator=";"))


if __name__ == "__main__":
    cfg = MainCfg()

    # fmt: off
    # flake8: noqa
    print(f"cfg.boxed_int ({type(cfg.boxed_int)}): {repr(cfg.boxed_int)}")
    print(f"cfg.boxed_int.item ({type(cfg.boxed_int.item)}): {repr(cfg.boxed_int.item)}")
    print(f"cfg.boxed_bytes ({type(cfg.boxed_bytes)}): {repr(cfg.boxed_bytes)}")
    print(f"cfg.boxed_bytes.item ({type(cfg.boxed_bytes.item)}): {repr(cfg.boxed_bytes.item)}")
    print(f"cfg.default_boolean ({type(cfg.default_boolean)}): {repr(cfg.default_boolean)}")
    print(f"cfg.custom_boolean ({type(cfg.custom_boolean)}): {repr(cfg.custom_boolean)}")
    print(f"cfg.required_list ({type(cfg.required_list)}): {repr(cfg.required_list)}")
    print(f"cfg.required_empty_list ({type(cfg.required_empty_list)}): {repr(cfg.required_empty_list)}")
    print(f"cfg.optional_list ({type(cfg.optional_list)}): {repr(cfg.optional_list)}")
    print(f"cfg.optional_empty_list ({type(cfg.optional_empty_list)}): {repr(cfg.optional_empty_list)}")
    print(f"cfg.optional_not_provided_list ({type(cfg.optional_not_provided_list)}): {repr(cfg.optional_not_provided_list)}")
    print(f"cfg.custom_list ({type(cfg.custom_list)}): {repr(cfg.custom_list)}")
    # fmt: on
```

Run the example with the environment set:

```bash
BOXED_INT=1 \
BOXED_BYTES='some_bytes' \
DEFAULT_BOOLEAN='T' \
CUSTOM_BOOLEAN='uhHuH' \
REQUIRED_LIST='1, 2, 3' \
OPTIONAL_LIST='1, 2, 3' \
REQUIRED_EMPTY_LIST='' \
OPTIONAL_EMPTY_LIST='' \
CUSTOM_LIST='123;456' \
python -m envenom.examples.parsers
```

```text
cfg.boxed_int (<class '__main__.Box'>): Box(item=1)
cfg.boxed_int.item (<class 'int'>): 1
cfg.boxed_bytes (<class '__main__.Box'>): Box(item=b'some_bytes')
cfg.boxed_bytes.item (<class 'bytes'>): b'some_bytes'
cfg.default_boolean (<class 'bool'>): True
cfg.custom_boolean (<class 'bool'>): False
cfg.required_list (<class 'list'>): ['1', ' 2', ' 3']
cfg.required_empty_list (<class 'list'>): []
cfg.optional_list (<class 'list'>): ['1', ' 2', ' 3']
cfg.optional_empty_list (<class 'list'>): []
cfg.optional_not_provided_list (<class 'NoneType'>): None
cfg.custom_list (<class 'list'>): [123, 456]
```
