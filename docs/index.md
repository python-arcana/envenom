<!-- `envenom` - an elegant application configurator for the more civilized age
Copyright (C) 2024 Artur Ciesielski <artur.ciesielski@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>. -->

[![pipeline status](https://gitlab.com/arcanery/python/envenom/badges/main/pipeline.svg)](https://gitlab.com/arcanery/python/envenom/-/commits/main)
[![coverage report](https://gitlab.com/arcanery/python/envenom/badges/main/coverage.svg)](https://gitlab.com/arcanery/python/envenom/-/commits/main)
[![latest release](https://gitlab.com/arcanery/python/envenom/-/badges/release.svg)](https://gitlab.com/arcanery/python/envenom/-/releases)

## Introduction

`envenom` is an elegant application configurator for the more civilized age.

`envenom` is written with simplicity and type safety in mind. It allows
you to express your application configuration declaratively in a dataclass-like
format while providing your application with type information about each entry,
its nullability and default values.

`envenom` is designed for modern usecases, allowing for pulling configuration from
environment variables or files for more sophisticated deployments on platforms
like Kubernetes - all in the spirit of [12factor](https://12factor.net/).

## How it works

An `envenom` config class looks like a regular Python dataclass - because it is one.

The `@envenom.config` decorator creates a new dataclass by converting the config fields
into their `dataclass` equivalents providing the relevant default field parameters. All
config classes created that way are marked as `frozen=True` - because the config should
not change mid-flight - and `eq=True`.

This also means it's 100% compatible with dataclasses. You can:

- use a config class as a property of a regular dataclass
- use a regular dataclass as a property of a config class
- declare static or dynamic fields using standard dataclass syntax
- use the `InitVar`/`__post_init__` method for delayed initialization of fields
- use methods, `classmethod`s, `staticmethod`s, and properties

`envenom` will automatically fetch the environment variable values to populate
dataclass fields (optionally running parsers so that fields are automatically
converted to desired types). This works out of the box with all types trivially
convertible from `str`, like `Enum` and `UUID`, and with any object type that can be
instantiated easily from a single string (any function `(str,) -> T` will work as a
parser).

If using a static type checker the type deduction system will correctly identify most
mistakes if you declare fields, parsers or default values with mismatched types. There
are certain exceptions, for example `T` will always satisfy type bounds `T | None`.

`envenom` also offers reading variable contents from file by specifying an environment
variable with the suffix `__FILE` which contains the path to a file with the respective
secret. This aims to facilitate a common deploy pattern where secrets are mounted as
files (especially prevalent with Kubernetes).

All interaction with the environment is case-sensitive - we'll convert everything to
uppercase, and since `_` is a common separator within environment variable names we use
`_` to replace any and all nonsensical characters, then use `__` to separate namespaces.
Therefore a field `"var"` in namespaces `("ns-1", "ns2")` will be mapped to
`NS_1__NS2__VAR`.

## What `envenom` isn't

`envenom` has a clearly defined scope limited to configuration management from the
application's point of view.

This means `envenom` is only interested in converting the environment into application
configuration and does not care about how the environment gets populated in the first place.

Things that are out of scope for `envenom` include, but are not limited to:

- injecting the environment into the runtime or orchestrator
- retrieving configuration or secrets from the cloud or another storage
(AWS Parameter/Secret Store, Azure Key Vault, HashiCorp Vault, etc.)
- retrieving and parsing configuration from structured config files (`YAML`/`JSON`/`INI` etc.)
