# `envenom` - an elegant application configurator for the more civilized age
# Copyright (C) 2024-2025 Artur Ciesielski <artur.ciesielski@gmail.com>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

[build-system]
requires = ["poetry-core>2.0.0"]
build-backend = "poetry.core.masonry.api"

[project]
name = "envenom"
description = "An elegant application configurator for the more civilized age"
version = "3.0.0"
requires-python = ">=3.12,<4.0"
license = "GPL-3.0-or-later"
authors = [{ name = "Artur Ciesielski", email = "artur.ciesielski@gmail.com" }]
readme = "README.md"
keywords = ["env", "environment", "config", "configuration"]
classifiers = [
    "Development Status :: 5 - Production/Stable",
    "Intended Audience :: Developers",
    "Operating System :: OS Independent",
    "Topic :: Software Development :: Libraries :: Application Frameworks",
    "Topic :: Software Development :: Libraries :: Python Modules",
    "Typing :: Typed",
]

[project.urls]
homepage = "https://gitlab.com/arcanery/python/envenom"
repository = "https://gitlab.com/arcanery/python/envenom"
documentation = "https://arcanery.gitlab.io/python/envenom/"


[tool.poetry.group.dev.dependencies]
poetry = "^2.1.1"


[tool.poetry.group.lint.dependencies]
pre-commit = "^4.1.0"


[tool.poetry.group.test.dependencies]
pytest = "^8.3.4"
pytest-lazy-fixtures = "^1.1.2"
coverage = "^7.6.12"


[tool.poetry.group.docs.dependencies]
mkdocs-material = "^9.6.5"
mkdocstrings-python = "^1.16.1"


[tool.isort]
profile = "black"


[tool.flake8]
max-line-length = 88


[tool.pyright]
typeCheckingMode = "strict"
exclude = [ "src/*/examples/*" ]


[tool.bandit]
exclude_dirs = [ "tests", "src/*/examples/*" ]


[tool.coverage.run]
branch = true
include = [ "src/*" ]
omit = [ "src/*/examples/*" ]


[tool.coverage.report]
exclude_also = [
    # Don't complain about abstract methods, they aren't run:
    "@(abc\\.)?abstractmethod",
    # Don't complain if tests don't hit defensive assertion code:
    "raise NotImplementedError",
    # Protocol classes cannot be instantiated, and therefore often escape coverage
    "class .*\\bProtocol\\):",
]
