# `envenom` - an elegant application configurator for the more civilized age
# Copyright (C) 2024-2025 Artur Ciesielski <artur.ciesielski@gmail.com>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import os
from collections.abc import Iterator
from unittest.mock import patch

import pytest

type Environment = dict[str, str]


@pytest.fixture(scope="function")
def environment() -> Environment:
    return {}


@pytest.fixture(scope="function")
def patched_environment(environment: Environment) -> Iterator[None]:
    with patch.dict(os.environ, environment):
        yield
