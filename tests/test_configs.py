# `envenom` - an elegant application configurator for the more civilized age
# Copyright (C) 2024-2025 Artur Ciesielski <artur.ciesielski@gmail.com>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import enum
from dataclasses import dataclass, field
from functools import cached_property
from uuid import UUID, uuid1

import pytest

from envenom import (
    config,
    optional,
    required,
    subconfig,
    with_default,
    with_default_factory,
)
from envenom.errors import MissingConfiguration
from envenom.parsers import make_bool_parser, make_list_parser

pytestmark = pytest.mark.usefixtures("patched_environment")

load_test_environment = pytest.mark.parametrize(
    "environment",
    (
        {
            "TEST_STR_OPTIONAL_SET": "optional",
            "TEST_STR_REQUIRED_SET": "required",
            "TEST_STR_WITH_DEFAULT_SET": "default",
            "TEST_INT_OPTIONAL_SET": "420",
            "TEST_INT_REQUIRED_SET": "420",
            "TEST_INT_WITH_DEFAULT_SET": "420",
            "TEST_BOOL_OPTIONAL_SET": "t",
            "TEST_BOOL_REQUIRED_SET": "y",
            "TEST_BOOL_WITH_DEFAULT_SET": "+",
            "TEST_UUID_OPTIONAL_SET": "1c13fa08-25e4-47de-ae90-f811cfd0e442",
            "TEST_UUID_REQUIRED_SET": "1c13fa08-25e4-47de-ae90-f811cfd0e442",
            "TEST_UUID_WITH_DEFAULT_SET": "1c13fa08-25e4-47de-ae90-f811cfd0e442",
            "TEST_ENUM_OPTIONAL_SET": "0",
            "TEST_ENUM_REQUIRED_SET": "0",
            "TEST_ENUM_WITH_DEFAULT_SET": "0",
            "TEST_LIST_OF_COMMA_SEPARATED_STRS": "test,2137",
            "TEST_LIST_OF_COLON_SEPARATED_INTS": "1:2:33:444",
        },
    ),
)


class ExitCode(enum.IntEnum):
    OK = 0
    ERROR = 1


@dataclass
class PlainOldDataclass:
    scalar_value: int = 1
    object_value: list[str] = field(default_factory=list)


@config()
class ClassAttributesCfg:
    @staticmethod
    def some_static_method() -> str:
        return "some static method value"

    @classmethod
    def some_class_method(cls) -> str:
        return "some class method value"

    @property
    def some_property(self) -> str:
        return "some property value"

    @cached_property
    def some_cached_property(self) -> str:
        return "some cached property value"


@config()
class IntCfg:
    test_int_optional_set: int | None = optional(int)
    test_int_optional_not_set: int | None = optional(int)
    test_int_required_set: int = required(int)
    test_int_with_default_set: int = with_default(int, default=2137)
    test_int_with_default_not_set: int = with_default(int, default=2137)


@config()
class StrCfg:
    test_str_optional_set: str | None = optional()
    test_str_optional_not_set: str | None = optional()
    test_str_required_set: str = required()
    test_str_with_default_set: str = with_default(default="<unknown>")
    test_str_with_default_not_set: str = with_default(default="<unknown>")


@config()
class BoolCfg:
    test_bool_optional_set: bool | None = optional(make_bool_parser())
    test_bool_optional_not_set: bool | None = optional(make_bool_parser())
    test_bool_required_set: bool = required(make_bool_parser())
    test_bool_with_default_set: bool = with_default(make_bool_parser(), default=False)
    test_bool_with_default_not_set: bool = with_default(
        make_bool_parser(), default=False
    )


@config()
class UUIDCfg:
    test_uuid_optional_set: UUID | None = optional(UUID)
    test_uuid_optional_not_set: UUID | None = optional(UUID)
    test_uuid_required_set: UUID = required(UUID)
    test_uuid_with_default_set: UUID = with_default_factory(UUID, default_factory=uuid1)
    test_uuid_with_default_not_set: UUID = with_default_factory(
        UUID, default_factory=uuid1
    )


@config()
class EnumCfg:
    test_enum_optional_set: ExitCode | None = optional(lambda s: ExitCode(int(s)))
    test_enum_optional_not_set: ExitCode | None = optional(lambda s: ExitCode(int(s)))
    test_enum_required_set: ExitCode = required(lambda s: ExitCode(int(s)))
    test_enum_with_default_set: ExitCode = with_default(
        lambda s: ExitCode(int(s)), default=ExitCode.ERROR
    )
    test_enum_with_default_not_set: ExitCode = with_default(
        lambda s: ExitCode(int(s)), default=ExitCode.ERROR
    )


@config()
class ListCfg:
    test_list_of_comma_separated_strs: list[str] = required(make_list_parser())
    test_list_of_colon_separated_ints: list[int] = required(
        make_list_parser(int, separator=":")
    )


@config()
class MainCfg:
    int_cfg: IntCfg = subconfig(IntCfg)
    str_cfg: StrCfg = subconfig(StrCfg)
    bool_cfg: BoolCfg = subconfig(BoolCfg)
    uuid_cfg: UUIDCfg = subconfig(UUIDCfg)
    enum_cfg: EnumCfg = subconfig(EnumCfg)
    list_cfg: ListCfg = subconfig(ListCfg)
    dataclass_cfg: PlainOldDataclass = subconfig(PlainOldDataclass)
    class_attributes_cfg: ClassAttributesCfg = subconfig(ClassAttributesCfg)


@load_test_environment
def test_simple_config_types_and_composition() -> None:
    cfg = MainCfg()

    assert cfg.int_cfg.test_int_optional_set == 420
    assert cfg.int_cfg.test_int_optional_not_set is None
    assert cfg.int_cfg.test_int_required_set == 420
    assert cfg.int_cfg.test_int_with_default_set == 420
    assert cfg.int_cfg.test_int_with_default_not_set == 2137

    assert cfg.str_cfg.test_str_optional_set == "optional"
    assert cfg.str_cfg.test_str_optional_not_set is None
    assert cfg.str_cfg.test_str_required_set == "required"
    assert cfg.str_cfg.test_str_with_default_set == "default"
    assert cfg.str_cfg.test_str_with_default_not_set == "<unknown>"

    assert cfg.bool_cfg.test_bool_optional_set is True
    assert cfg.bool_cfg.test_bool_optional_not_set is None
    assert cfg.bool_cfg.test_bool_required_set is True
    assert cfg.bool_cfg.test_bool_with_default_set is True
    assert cfg.bool_cfg.test_bool_with_default_not_set is False

    assert cfg.uuid_cfg.test_uuid_optional_set == UUID(
        "1c13fa08-25e4-47de-ae90-f811cfd0e442"
    )
    assert cfg.uuid_cfg.test_uuid_optional_not_set is None
    assert cfg.uuid_cfg.test_uuid_required_set == UUID(
        "1c13fa08-25e4-47de-ae90-f811cfd0e442"
    )
    assert cfg.uuid_cfg.test_uuid_with_default_set == UUID(
        "1c13fa08-25e4-47de-ae90-f811cfd0e442"
    )
    assert isinstance(cfg.uuid_cfg.test_uuid_with_default_not_set, UUID)

    assert cfg.enum_cfg.test_enum_optional_set == ExitCode.OK
    assert cfg.enum_cfg.test_enum_optional_not_set is None
    assert cfg.enum_cfg.test_enum_required_set == ExitCode.OK
    assert cfg.enum_cfg.test_enum_with_default_set == ExitCode.OK
    assert cfg.enum_cfg.test_enum_with_default_not_set == ExitCode.ERROR

    assert cfg.list_cfg.test_list_of_comma_separated_strs == ["test", "2137"]
    assert cfg.list_cfg.test_list_of_colon_separated_ints == [1, 2, 33, 444]

    assert cfg.dataclass_cfg.scalar_value == 1
    assert cfg.dataclass_cfg.object_value == []

    assert cfg.class_attributes_cfg.some_static_method() == "some static method value"
    assert cfg.class_attributes_cfg.some_class_method() == "some class method value"
    assert cfg.class_attributes_cfg.some_property == "some property value"
    assert cfg.class_attributes_cfg.some_cached_property == "some cached property value"

    assert ClassAttributesCfg.some_static_method() == "some static method value"
    assert ClassAttributesCfg.some_class_method() == "some class method value"


@load_test_environment
def test_required_cfg_field_unset() -> None:

    @config(namespace="nonexistent")
    class RequiredUnsetCfg:
        nonexistent: str = required()

    with pytest.raises(MissingConfiguration):
        RequiredUnsetCfg()
