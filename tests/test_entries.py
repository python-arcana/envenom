# `envenom` - an elegant application configurator for the more civilized age
# Copyright (C) 2024-2025 Artur Ciesielski <artur.ciesielski@gmail.com>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import pytest

from envenom.entries import EntryWithDefault, OptionalEntry, RequiredEntry
from envenom.vars import OptionalVar, RequiredVar, VarWithDefault


@pytest.fixture(scope="function")
def optional_entry() -> OptionalEntry[int]:
    return OptionalEntry(parser=int)


@pytest.fixture(scope="function")
def required_entry() -> RequiredEntry[int]:
    return RequiredEntry(parser=int)


@pytest.fixture(scope="function")
def entry_with_default() -> EntryWithDefault[int]:
    return EntryWithDefault(parser=int, default=2137)


def test_optional_entry(optional_entry: OptionalEntry[int]) -> None:
    assert optional_entry.get_var("some_name", "some_namespace") == OptionalVar(
        "some_name", "some_namespace", parser=int, file=True
    )


def test_required_entry(required_entry: RequiredEntry[int]) -> None:
    assert required_entry.get_var("some_name", "some_namespace") == RequiredVar(
        "some_name", "some_namespace", parser=int, file=True
    )


def test_entry_with_default(entry_with_default: EntryWithDefault[int]) -> None:
    assert entry_with_default.get_var("some_name", "some_namespace") == VarWithDefault(
        "some_name", "some_namespace", parser=int, file=True, default=2137
    )
