# `envenom` - an elegant application configurator for the more civilized age
# Copyright (C) 2024-2025 Artur Ciesielski <artur.ciesielski@gmail.com>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import pytest
from pytest_lazy_fixtures import lf

from envenom.errors import (
    ConfigurationError,
    ConfigurationFileUnreadable,
    GenericError,
    InvalidConfiguration,
    MissingConfiguration,
)


@pytest.fixture(scope="session")
def configuration_file_unreadable() -> ConfigurationFileUnreadable:
    return ConfigurationFileUnreadable("EXAMPLE_NAMESPACE__EXAMPLE_VAR", "420")


@pytest.fixture(scope="session")
def invalid_configuration_error() -> InvalidConfiguration:
    return InvalidConfiguration("EXAMPLE_NAMESPACE__EXAMPLE_VAR", "420")


@pytest.fixture(scope="session")
def missing_configuration_error() -> MissingConfiguration:
    return MissingConfiguration("EXAMPLE_NAMESPACE__EXAMPLE_VAR")


@pytest.fixture(scope="session")
def configuration_error() -> ConfigurationError:
    return ConfigurationError("EXAMPLE_NAMESPACE__EXAMPLE_VAR")


@pytest.fixture(scope="session")
def generic_error() -> GenericError:
    return GenericError()


def test_generic_error_representation(generic_error: GenericError) -> None:
    assert str(generic_error) == "error"
    assert repr(generic_error) == "GenericError()"
    assert format(generic_error) == "error"
    assert format(generic_error, "class") == "GenericError"


@pytest.mark.parametrize(
    (
        "error",
        "expected_str",
        "expected_repr",
        "expected_format_class",
        "expected_format_var",
    ),
    (
        (
            lf("configuration_file_unreadable"),
            "unreadable file '420' for configuration 'EXAMPLE_NAMESPACE__EXAMPLE_VAR'",
            (
                "ConfigurationFileUnreadable("
                "name='EXAMPLE_NAMESPACE__EXAMPLE_VAR', value='420')"
            ),
            "ConfigurationFileUnreadable",
            "EXAMPLE_NAMESPACE__EXAMPLE_VAR",
        ),
        (
            lf("invalid_configuration_error"),
            "invalid value '420' for configuration 'EXAMPLE_NAMESPACE__EXAMPLE_VAR'",
            "InvalidConfiguration(name='EXAMPLE_NAMESPACE__EXAMPLE_VAR', value='420')",
            "InvalidConfiguration",
            "EXAMPLE_NAMESPACE__EXAMPLE_VAR",
        ),
        (
            lf("missing_configuration_error"),
            "missing value for configuration 'EXAMPLE_NAMESPACE__EXAMPLE_VAR'",
            "MissingConfiguration(name='EXAMPLE_NAMESPACE__EXAMPLE_VAR')",
            "MissingConfiguration",
            "EXAMPLE_NAMESPACE__EXAMPLE_VAR",
        ),
        (
            lf("configuration_error"),
            "error for configuration 'EXAMPLE_NAMESPACE__EXAMPLE_VAR'",
            "ConfigurationError(name='EXAMPLE_NAMESPACE__EXAMPLE_VAR')",
            "ConfigurationError",
            "EXAMPLE_NAMESPACE__EXAMPLE_VAR",
        ),
    ),
    ids=(
        "ConfigurationFileUnreadable",
        "InvalidConfiguration",
        "MissingConfiguration",
        "ConfigurationError",
    ),
)
def test_configuration_error_representation(
    error: ConfigurationError,
    expected_str: str,
    expected_repr: str,
    expected_format_class: str,
    expected_format_var: str,
) -> None:
    assert str(error) == expected_str
    assert repr(error) == expected_repr
    assert format(error) == expected_str
    assert format(error, "class") == expected_format_class
    assert format(error, "var") == expected_format_var


@pytest.mark.parametrize(
    (
        "error",
        "expected_format_value",
    ),
    (
        (
            lf("configuration_file_unreadable"),
            "420",
        ),
        (
            lf("invalid_configuration_error"),
            "420",
        ),
    ),
    ids=(
        "ConfigurationFileUnreadable",
        "InvalidConfiguration",
    ),
)
def test_error_representation_for_value_format_spec(
    error: ConfigurationError,
    expected_format_value: str,
) -> None:
    assert format(error, "value") == expected_format_value
