# `envenom` - an elegant application configurator for the more civilized age
# Copyright (C) 2024-2025 Artur Ciesielski <artur.ciesielski@gmail.com>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from itertools import repeat

import pytest

from envenom.parsers import Parser, make_bool_parser

TRUE_VALUES = ("true", "tRuE", "t", "T", "Yes", "yes", "y", "Y", "1", "+", "✓")
FALSE_VALUES = ("false", "FaLSe", "f", "F", "nO", "no", "n", "N", "0", "-", "✗")
ERROR_VALUES = ("", "213", "♠", "s", "treu", "ye", "nOpe", "nop", "nn")


@pytest.fixture(scope="session")
def default_bool_parser() -> Parser[bool]:
    return make_bool_parser()


@pytest.mark.parametrize(
    ("v", "expected"),
    (
        *zip(TRUE_VALUES, repeat(True)),
        *zip(FALSE_VALUES, repeat(False)),
    ),
)
def test_bool_parser(v: str, expected: bool, default_bool_parser: Parser[bool]) -> None:
    assert default_bool_parser(v) is expected


@pytest.mark.parametrize("v", ERROR_VALUES)
def test_bool_parser_errors(v: str, default_bool_parser: Parser[bool]) -> None:
    with pytest.raises(ValueError):
        default_bool_parser(v)


def test_bool_parser_custom_values() -> None:
    bool_parser = make_bool_parser(true_values={"a"}, false_values={"b"})

    assert bool_parser("a") is True
    assert bool_parser("A") is True
    assert bool_parser("b") is False
    assert bool_parser("B") is False
    with pytest.raises(ValueError):
        bool_parser("t")
    with pytest.raises(ValueError):
        bool_parser("f")
