# `envenom` - an elegant application configurator for the more civilized age
# Copyright (C) 2024-2025 Artur Ciesielski <artur.ciesielski@gmail.com>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import pytest

from envenom.parsers import Parser, make_list_parser


@pytest.fixture(scope="session")
def default_list_parser() -> Parser[list[str]]:
    return make_list_parser()


@pytest.fixture(scope="session")
def semicolon_separated_list_parser() -> Parser[list[str]]:
    return make_list_parser(separator=";")


@pytest.fixture(scope="session")
def int_list_parser() -> Parser[list[int]]:
    return make_list_parser(int)


@pytest.fixture(scope="session")
def semicolon_separated_int_list_parser() -> Parser[list[int]]:
    return make_list_parser(int, separator=";")


def test_list_parser_empty_parser(default_list_parser: Parser[list[str]]) -> None:
    assert default_list_parser("") == []


def test_list_parser_singleton_parser(default_list_parser: Parser[list[str]]) -> None:
    assert default_list_parser("1") == ["1"]


def test_list_parser_of_str_parser(default_list_parser: Parser[list[str]]) -> None:
    assert default_list_parser("1,2,3,4") == ["1", "2", "3", "4"]


def test_list_parser_of_str_custom_separator_parser(
    semicolon_separated_list_parser: Parser[list[str]],
) -> None:
    assert semicolon_separated_list_parser("1;2,3;4") == ["1", "2,3", "4"]


def test_list_parser_of_int_parser(int_list_parser: Parser[list[int]]) -> None:
    assert int_list_parser("1,2,3,4") == [1, 2, 3, 4]


def test_list_parser_errors(
    semicolon_separated_int_list_parser: Parser[list[int]],
) -> None:
    with pytest.raises(ValueError) as cm:
        semicolon_separated_int_list_parser("1;test")
    assert cm.value.args == ("invalid literal for int() with base 10: 'test'",)
