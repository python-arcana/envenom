# `envenom` - an elegant application configurator for the more civilized age
# Copyright (C) 2024-2025 Artur Ciesielski <artur.ciesielski@gmail.com>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import pathlib

import pytest

from envenom.vars import (
    Namespace,
    OptionalVar,
    RequiredVar,
    VarWithDefault,
    VarWithDefaultFactory,
)


class NamespaceFixtureRequest:
    param: Namespace


@pytest.fixture(scope="function")
def namespace(request: NamespaceFixtureRequest) -> Namespace:
    return getattr(request, "param", "TEST_NAMESPACE")


class VarFileContentFixtureRequest:
    param: str | None


@pytest.fixture(scope="function")
def var_file_contents(request: VarFileContentFixtureRequest) -> str | None:
    return getattr(request, "param", None)


@pytest.fixture(scope="function")
def var_file_path(tmp_path: pathlib.Path, var_file_contents: str | None) -> str:
    vars_dir = tmp_path / "vars"
    vars_dir.mkdir(parents=True, exist_ok=True)
    path = vars_dir / "var_file.txt"
    path.touch(exist_ok=False)
    if var_file_contents is not None:
        path.write_text(var_file_contents)
    return str(path)


class VarTryFileFixtureRequest:
    param: bool = False


@pytest.fixture(scope="function")
def var_try_file(request: VarTryFileFixtureRequest) -> bool:
    return getattr(request, "param", True)


@pytest.fixture(scope="function")
def optional_var(namespace: Namespace, var_try_file: bool) -> OptionalVar[int]:
    return OptionalVar("optional_var", namespace, parser=int, file=var_try_file)


@pytest.fixture(scope="function")
def required_var(namespace: Namespace, var_try_file: bool) -> RequiredVar[int]:
    return RequiredVar("required_var", namespace, parser=int, file=var_try_file)


@pytest.fixture(scope="function")
def var_with_default(namespace: Namespace, var_try_file: bool) -> VarWithDefault[int]:
    return VarWithDefault(
        "var_with_default",
        namespace,
        parser=int,
        default=2137,
        file=var_try_file,
    )


@pytest.fixture(scope="function")
def var_with_default_factory(
    namespace: Namespace, var_try_file: bool
) -> VarWithDefaultFactory[int]:
    return VarWithDefaultFactory(
        "var_with_default_factory",
        namespace,
        parser=int,
        default_factory=lambda: 2137,
        file=var_try_file,
    )
