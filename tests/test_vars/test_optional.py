# `envenom` - an elegant application configurator for the more civilized age
# Copyright (C) 2024-2025 Artur Ciesielski <artur.ciesielski@gmail.com>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import pytest
from pytest_lazy_fixtures import lf

from envenom.errors import ConfigurationFileUnreadable, InvalidConfiguration
from envenom.vars import OptionalVar

parametrize_correct_namespaces = pytest.mark.parametrize(
    "namespace",
    ("TEST_NAMESPACE", ("TEST_NAMESPACE",)),
    ids=("TEST_NAMESPACE", "tuple(TEST_NAMESPACE)"),
    indirect=True,
)
parametrize_wrong_namespaces = pytest.mark.parametrize(
    "namespace",
    (None, "TEST_NAMESPACE_2", ("TEST_NAMESPACE_2",)),
    ids=(None, "TEST_NAMESPACE_2", "tuple(TEST_NAMESPACE_2)"),
    indirect=True,
)


@pytest.mark.usefixtures("patched_environment")
@pytest.mark.parametrize(
    ("environment", "expected"),
    (
        ({"TEST_NAMESPACE__OPTIONAL_VAR": "420"}, 420),
        ({}, None),
    ),
    ids=("custom_environment", "default_enviroment"),
)
@parametrize_correct_namespaces
def test_optional_var(optional_var: OptionalVar[int], expected: int | None) -> None:
    assert optional_var.get() == expected


@pytest.mark.usefixtures("patched_environment")
@pytest.mark.parametrize(
    "environment",
    ({"TEST_NAMESPACE__OPTIONAL_VAR": "420"},),
)
@parametrize_wrong_namespaces
def test_optional_var_wrong_namespace(optional_var: OptionalVar[int]) -> None:
    assert optional_var.get() is None


@pytest.mark.usefixtures("patched_environment")
@pytest.mark.parametrize(
    "environment",
    ({"TEST_NAMESPACE__OPTIONAL_VAR": "xxx"},),
)
@parametrize_correct_namespaces
def test_optional_var_bad_value(optional_var: OptionalVar[int]) -> None:
    with pytest.raises(InvalidConfiguration) as exc_info:
        optional_var.get()
    assert exc_info.value == InvalidConfiguration("TEST_NAMESPACE__OPTIONAL_VAR", "xxx")


@pytest.mark.usefixtures("patched_environment", "var_file_path")
@pytest.mark.parametrize("var_file_contents", ("420",), indirect=True)
@pytest.mark.parametrize(
    "environment",
    ({"TEST_NAMESPACE__OPTIONAL_VAR__FILE": lf("var_file_path")},),
)
@parametrize_correct_namespaces
def test_optional_var_file(optional_var: OptionalVar[int]) -> None:
    assert optional_var.get() == 420


@pytest.mark.usefixtures("patched_environment")
@pytest.mark.parametrize(
    "environment",
    ({"TEST_NAMESPACE__OPTIONAL_VAR__FILE": "/tmp/nope.txt"},),
)
@parametrize_correct_namespaces
def test_optional_var_file_unreadable(optional_var: OptionalVar[int]) -> None:
    with pytest.raises(ConfigurationFileUnreadable) as exc_info:
        optional_var.get()
    assert exc_info.value == ConfigurationFileUnreadable(
        "TEST_NAMESPACE__OPTIONAL_VAR", "/tmp/nope.txt"
    )


@pytest.mark.usefixtures("patched_environment")
@pytest.mark.parametrize("var_try_file", (False,))
@pytest.mark.parametrize(
    ("environment", "expected"),
    (
        (
            {
                "TEST_NAMESPACE__OPTIONAL_VAR": "420",
                "TEST_NAMESPACE__OPTIONAL_VAR__FILE": "/tmp/nope.txt",
            },
            420,
        ),
        (
            {
                "TEST_NAMESPACE__OPTIONAL_VAR__FILE": "/tmp/nope.txt",
            },
            None,
        ),
    ),
)
@parametrize_correct_namespaces
def test_optional_var_no_file(
    optional_var: OptionalVar[int], expected: int | None
) -> None:
    assert optional_var.get() == expected
