# `envenom` - an elegant application configurator for the more civilized age
# Copyright (C) 2024-2025 Artur Ciesielski <artur.ciesielski@gmail.com>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import pytest
from pytest_lazy_fixtures import lf

from envenom.errors import ConfigurationFileUnreadable, InvalidConfiguration
from envenom.vars import VarWithDefaultFactory

parametrize_correct_namespaces = pytest.mark.parametrize(
    "namespace",
    ("TEST_NAMESPACE", ("TEST_NAMESPACE",)),
    ids=("TEST_NAMESPACE", "tuple(TEST_NAMESPACE)"),
    indirect=True,
)
parametrize_wrong_namespaces = pytest.mark.parametrize(
    "namespace",
    (None, "TEST_NAMESPACE_2", ("TEST_NAMESPACE_2",)),
    ids=(None, "TEST_NAMESPACE_2", "tuple(TEST_NAMESPACE_2)"),
    indirect=True,
)


@pytest.mark.usefixtures("patched_environment")
@pytest.mark.parametrize(
    ("environment", "expected"),
    (
        ({"TEST_NAMESPACE__VAR_WITH_DEFAULT_FACTORY": "420"}, 420),
        ({}, 2137),
    ),
    ids=("custom_environment", "default_enviroment"),
)
@parametrize_correct_namespaces
def test_var_with_default_factory(
    var_with_default_factory: VarWithDefaultFactory[int], expected: int
) -> None:
    assert var_with_default_factory.get() == expected


@pytest.mark.usefixtures("patched_environment")
@pytest.mark.parametrize(
    "environment",
    ({"TEST_NAMESPACE__VAR_WITH_DEFAULT_FACTORY": "420"},),
)
@parametrize_wrong_namespaces
def test_var_with_default_factory_wrong_namespace(
    var_with_default_factory: VarWithDefaultFactory[int],
) -> None:
    assert var_with_default_factory.get() == 2137


@pytest.mark.usefixtures("patched_environment")
@pytest.mark.parametrize(
    "environment",
    ({"TEST_NAMESPACE__VAR_WITH_DEFAULT_FACTORY": "xxx"},),
)
@parametrize_correct_namespaces
def test_var_with_default_factory_bad_value(
    var_with_default_factory: VarWithDefaultFactory[int],
) -> None:
    with pytest.raises(InvalidConfiguration) as exc_info:
        var_with_default_factory.get()
    assert exc_info.value == InvalidConfiguration(
        "TEST_NAMESPACE__VAR_WITH_DEFAULT_FACTORY", "xxx"
    )


@pytest.mark.usefixtures("patched_environment", "var_file_path")
@pytest.mark.parametrize("var_file_contents", ("420",), indirect=True)
@pytest.mark.parametrize(
    "environment",
    ({"TEST_NAMESPACE__VAR_WITH_DEFAULT_FACTORY__FILE": lf("var_file_path")},),
)
@parametrize_correct_namespaces
def test_var_with_default_factory_file(
    var_with_default_factory: VarWithDefaultFactory[int],
) -> None:
    assert var_with_default_factory.get() == 420


@pytest.mark.usefixtures("patched_environment")
@pytest.mark.parametrize(
    "environment",
    ({"TEST_NAMESPACE__VAR_WITH_DEFAULT_FACTORY__FILE": "/tmp/nope.txt"},),
)
@parametrize_correct_namespaces
def test_var_with_default_factory_file_unreadable(
    var_with_default_factory: VarWithDefaultFactory[int],
) -> None:
    with pytest.raises(ConfigurationFileUnreadable) as exc_info:
        var_with_default_factory.get()
    assert exc_info.value == ConfigurationFileUnreadable(
        "TEST_NAMESPACE__VAR_WITH_DEFAULT_FACTORY", "/tmp/nope.txt"
    )


@pytest.mark.usefixtures("patched_environment")
@pytest.mark.parametrize("var_try_file", (False,))
@pytest.mark.parametrize(
    ("environment", "expected"),
    (
        (
            {
                "TEST_NAMESPACE__VAR_WITH_DEFAULT_FACTORY": "420",
                "TEST_NAMESPACE__VAR_WITH_DEFAULT_FACTORY__FILE": "/tmp/nope.txt",
            },
            420,
        ),
        (
            {
                "TEST_NAMESPACE__VAR_WITH_DEFAULT_FACTORY__FILE": "/tmp/nope.txt",
            },
            2137,
        ),
    ),
)
@parametrize_correct_namespaces
def test_var_with_default_factory_no_file(
    var_with_default_factory: VarWithDefaultFactory[int], expected: int
) -> None:
    assert var_with_default_factory.get() == expected
